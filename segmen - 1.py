import numpy as np
import cv2
import time
import nibabel as nib
from mayavi import mlab

class Node(object):
    excessFlow = 0
    nodeHeight = 0
    nodeNumber = 0
    neighbor = []

class Edge(object):
    v = 0
    flow = 0

    def __init__(self, v, flow):
        self.v = v
        self.flow = flow

def preProcessing():
    global img
    global result
    global w, h
    global Ib, If
    global mroi
    global sroi
    global mean
    global stdv
    global HEIGHT_MAX
    size = 11
    kernel = np.ones((3,3), np.uint8)

    mean, stdv = cv2.meanStdDev(img)
    ut = mean + 1.5 * stdv
    lt = mean + 0.5 * stdv

    for i in range(height):
        for j in range(width):
            if not (img[i][j] > lt and img[i][j] < ut):
                img[i][j] = 0

    result = img.copy()
    cv2.equalizeHist(img, img)

    img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    img = cv2.pyrMeanShiftFiltering(img, 10, 50)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    cv2.medianBlur(img, size, img)
    cv2.medianBlur(img, size, img)

    mean, stdv = cv2.meanStdDev(img)
    t = mean + 1.5 * stdv

    for i in range(height):
        for j in range(width):
            if img[i][j] < t:
                img[i][j] = 0

    edges = cv2.Canny(img, 10, 100)

    # edges = cv2.dilate(edges, kernel)
    im2, contours, hierarchy = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    largest_index = 0
    true = 1
    while true and len(contours) > 0:
        largest_index = 0
        for i in range(0, len(contours)):
            a = cv2.contourArea(contours[i], 0)
            largest_area = cv2.contourArea(contours[largest_index], 0)
            if a > largest_area:
                largest_index = i

        if len(contours) > largest_index:
            if checkPosition(contours[largest_index]):
                true = 0
            else:
                del contours[largest_index]

    ref = np.zeros((height, width, 1), np.uint8)
    if len(contours) > largest_index:
        cnt = contours[largest_index]
        cv2.drawContours(ref, cnt, -1, (255), -1)
        x, y, w, h = cv2.boundingRect(cnt)
        HEIGHT_MAX = h*w
    else:
        x, y, w, h = 0,0,0,0
        HEIGHT_MAX = 0

    cv2.equalizeHist(result, img)
    # cv2.imwrite("hist" + `number` + "-" + `counter` + ".jpg", img)
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    img = cv2.pyrMeanShiftFiltering(img, 10, 50)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    cv2.medianBlur(img, 11, img)

    kernel = np.ones((5,5), np.uint8)
    mask = np.zeros((height + 2, width + 2), np.uint8)
    cv2.floodFill(ref, mask, (0, 0), 255)
    ref = cv2.bitwise_not(ref)
    ref = cv2.erode(ref, kernel)
    ref = cv2.erode(ref, kernel)
    ref = cv2.erode(ref, kernel)

    img = cv2.bitwise_and(img, img, mask=ref)

    ref = np.zeros((height, width, 1), np.uint8)
    result= cv2.rectangle(ref, (x, y), (x + w, y + h), (255), -1)

    data = [x for x in img.ravel() if x != 0]
    mean, stdv = np.mean(data), np.std(data)
    t = mean + stdv
    for i in range(height):
        for j in range(width):
            if img[i][j] > t:
                img[i][j] = 0

    a = height / 8
    b = width / 8

    data = [x for x in img.ravel() if x != 0]
    mean, stdv = np.mean(data), np.std(data)
    print mean, stdv
    for i in range(1, 9):
        for j in range(1, 9):
            n = (i - 1) * 8 + j
            ray = img[b * (i - 1):b * i, a * (j - 1):a * j]
            data = [x for x in ray.ravel() if x != 0]
            if len(data) > 0:
                m = np.mean(data)
                s = np.std(data)
                print n, m, s
                if s > stdv:
                    If = If + data
                else:
                    Ib = Ib + data

    mroi = 0
    sroi = 0
    mean = 0
    stdv = 0
    if len(If) > 0:
        mroi = np.mean(If)
        sroi = np.std(If)
    if len(Ib) > 0:
        mean = np.mean(Ib)
        stdv = np.std(Ib)

def checkPosition(cnt):
    M = cv2.moments(cnt)
    # print M["m01"], M["m00"]
    if M["m00"] != 0:
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
    else:
        cX = 0
        cY = 0
    rmid = width/2 + 0
    lmid = width/2 - 16

    if cX > rmid:# or cX < lmid:
        mask = np.zeros((height + 2, width + 2, 1), np.uint8)
        ref = np.zeros((height, width, 1), np.uint8)
        black = np.zeros((height, width), np.uint8)
        cv2.drawContours(ref, cnt, -1, (255), -1)

        cv2.floodFill(ref, mask, (0, 0), 255)
        ref = cv2.bitwise_not(ref)
        if len(liver) > 0:
            before = liver[-1]
            uni = cv2.bitwise_and(before, before, mask=ref)
            xaxis = [x for x in range(height * width)]
            mod = np.histogram(uni, xaxis)
            mod[0][0] = 0
            if not np.array_equal(before, black):
                if mod[0].argmax() != 0:
                    return 1
                else:
                    return 0
            else:
                return 1
        else:
            return 1
    else:
        return 0

def areaCalculation(segImg, refImg):
    ref = refImg.copy()
    img = segImg.copy()
    for i in range(height):
        for j in range(width):
            if ref[i][j] < 230:
                ref[i][j] = 0
            if img[i][j] < 230:
                img[i][j] = 0

    xaxis = [x for x in range(height*width)]
    mod = np.histogram(img, xaxis)
    area = mod[0][255] * xDim * yDim
    print ("area Img = %.2f" % area)
    areaImg.append(area)

    mod = np.histogram(ref, xaxis)
    area = mod[0][255] * xDim * yDim
    print ("area Ref = %.2f" % area)
    areaRef.append(area)

    intersect = cv2.bitwise_and(img, ref, mask=img)
    union = cv2.bitwise_or(img, ref, mask=img)

    mod = np.histogram(intersect, xaxis)
    areaIntersect.append(mod[0][255]*xDim*yDim)

    mod = np.histogram(union, xaxis)
    areaUnion.append(mod[0][255]*xDim*yDim)

def nlink(p, q):
    global stdv
    if p <= q:
        return 1
    else:
        return np.exp(-(((p - q) ** 2) / (2 * (stdv ** 2))))  # / np.sqrt(p*p+q*q)

def tlinksource(p, k):
    if p in If:
        lflow = k
    elif p in Ib:
        lflow = 0
    else:
        lflow = -np.log(abs(p - Ibmean) / (abs(p - Ifmean) + abs(p - Ibmean))) * 0.5

    return lflow

def tlinksink(p, k):
    if p in Ib:
        lflow = k
    elif p in If:
        lflow = 0
    else:
        lflow = np.exp(-(((p - mroi) ** 2) / (2 * (sroi ** 2)))) / (sroi*np.sqrt(2*np.pi))

    return lflow

def isActive(node):
    if node.excessFlow > 0 and node.nodeHeight < HEIGHT_MAX:
        return 1
    else:
        return 0

def push(node):
    global active
    global graph
    if isActive(node):
        for neighbor in node.neighbor:
            if graph[neighbor.v].nodeHeight == (node.nodeHeight - 1):
                lflow = min(neighbor.flow, node.excessFlow)
                node.excessFlow = node.excessFlow - lflow
                graph[neighbor.v].excessFlow = graph[neighbor.v].excessFlow + lflow
                neighbor.flow = neighbor.flow - lflow

                if not active:
                    active = 1

def relabel(node):
    global active
    global graph
    if isActive(node):
        height = HEIGHT_MAX
        for neighbor in node.neighbor:
            if neighbor.flow > 0:
                height = min(height, graph[neighbor.v].nodeHeight + 1)
            if not active:
                active = 1
        node.nodeHeight = height

def graphConstruction():
    global result
    global graph
    k = 1
    sum = 0
    maxsum = 0

    index = 0
    for i in range(height):
        for j in range(width):
            if (result[i][j] == 255):
                node = Node()
                node.nodeNumber = i * width + j
                node.neighbor = []
                sum = 0

                if i - 1 >= 0 and result[i - 1][j] == 255:
                    flow = nlink(img[i][j], img[i - 1][j])
                    node.neighbor.append(Edge(index - w, flow))
                    sum = sum + flow
                if j - 1 >= 0 and result[i][j - 1] == 255:
                    flow = nlink(img[i][j], img[i][j - 1])
                    node.neighbor.append(Edge(index - 1, flow))
                    sum = sum + flow
                if i + 1 < height and result[i + 1][j] == 255:
                    flow = nlink(img[i][j], img[i + 1][j])
                    node.neighbor.append(Edge(index + w, flow))
                    sum = sum + flow
                if j + 1 < width and result[i][j + 1] == 255:
                    flow = nlink(img[i][j], img[i][j + 1])
                    node.neighbor.append(Edge(index + 1, flow))
                    sum = sum + flow
                graph.append(node)
                index += 1
                if sum > maxsum:
                    maxsum = sum

    k = 1 + maxsum
    c = 0
    for i in range(height):
        for j in range(width):
            if (result[i][j] == 255):
                sf = tlinksource(img[i][j], k)
                tf = tlinksink(img[i][j], k)

                graph[c].excessFlow = sf - tf
                #node = graph[i * width + j]
                c +=1

def graphCut():
    global active
    global graph
    it = 0
    while (active and it < 100):
        active = 0
        for node in graph:
            relabel(node)

        for node in graph:
            push(node)
        it+=1

    index = 0
    for i in range(height):
        for j in range(width):
            if (result[i][j] == 255):
                if img[i][j] != 0:
                    if graph[index].nodeHeight == HEIGHT_MAX:
                        img[i][j] = 255
                    else:
                        img[i][j] = 128
                else:
                    img[i][j] = 0
                index+=1
            else:
                img[i][j] = 0

def analyze():
    volImg = sum(areaImg) * zDim
    volRef = sum(areaRef) * zDim
    volIntersect = sum(areaIntersect) * zDim
    volUnion = sum(areaUnion) * zDim
    dsc = (2*volIntersect/(volRef + volImg))
    voe = 1 - (volIntersect/volUnion)
    avd = abs(volImg - volRef)/volRef

    print ("areaRef = %.2f" % volRef)
    print ("areaImg = %.2f" % volImg)
    print ("areaIntersect = %.2f" % volIntersect)
    print ("areaUnion = %.2f" % volUnion)
    print ("dsc = %.4f" % dsc)
    print ("voe = %.4f" % voe)
    print ("avd = %.4f" % avd)

if __name__ == "__main__":
    '''Variabel Global'''
    start_time = time.time()

    number = 81
    zx = 241
    zy = 254
    image = nib.load('volume-'+ `number`+ '.nii')
    reference = nib.load('segmentation-'+`number`+'.nii')
    xDim = image.header['pixdim'][1]
    yDim = image.header['pixdim'][2]
    zDim = image.header['pixdim'][3]

    liver = []
    areaUnion = []
    areaIntersect = []
    areaImg = []
    areaRef = []
    If = []
    Ib = []

    for i in range(zx, zy):
        counter = i
        graph = []
        print "slice", i
        img = image.dataobj[:,:,i]
        ref = reference.dataobj[:,:,i]

        A = np.double(img)
        B = np.double(ref)

        outImg = np.zeros(A.shape, np.double)
        outRef = np.zeros(B.shape, np.double)
        normalizedImg = cv2.normalize(A, outImg, 1.0, 0.0, cv2.NORM_MINMAX)
        normalizedRef = cv2.normalize(B, outRef, 1.0, 0.0, cv2.NORM_MINMAX)

        img = normalizedImg*255
        ref = normalizedRef*255
        img = np.uint8(img)
        ref = np.uint8(ref)

        height, width = img.shape
        M = cv2.getRotationMatrix2D((width / 2, height / 2), 90, 1)

        img = cv2.warpAffine(img, M, (width, height))
        ref = cv2.warpAffine(ref, M, (width, height))

        xaxis = [x for x in range(height * width)]
        mode = np.histogram(ref, xaxis)
        if mode[0][127] == 0:
            for x in range(height):
                for y in range(width):
                    if ref[x][y] == 255:
                        ref[x][y] = 127

        If = []
        Ib = [0]
        active = 1

        preProcessing()
        # HEIGHT_MAX = 1
        # cv2.imwrite("ref81-"+`i`+".jpg", ref)

        Ifmean = np.mean(If)
        Ibmean = np.mean(Ib)

        graphConstruction()
        graphCut()
        areaCalculation(img, ref)
        liver.append(img)
        cv2.imwrite("img" + `number` + "-" + `i` + ".jpg", img)

    analyze()
    print("--- %s seconds ---" % (time.time() - start_time))

    # src = mlab.pipeline.scalar_field(liver)
    # mlab.pipeline.volume(src, vmin=0.4, vmax=0.8)
    mlab.contour3d(liver, opacity=.5)
    mlab.show()

    cv2.waitKey(0)

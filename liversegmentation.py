import cv2
import numpy as np
import matplotlib.pyplot as plt
import nibabel as nib
import stat
import matplotlib.axes as ax

# img = cv2.imread('volume-81-246.jpg', 0)
data = nib.load('volume-78.nii')
xDim = data.header['pixdim'][1]
yDim = data.header['pixdim'][2]

cek = data.dataobj[:,:,140]
height, width = cek.shape
A = np.double(cek)
outImg = np.zeros(A.shape, np.double)
normalizedImg = cv2.normalize(A, outImg, 1.0, 0.0, cv2.NORM_MINMAX)
cek = normalizedImg*255
cek = np.uint8(cek)
M = cv2.getRotationMatrix2D((width / 2, height / 2), 90, 1)
cek = cv2.warpAffine(cek, M, (width, height))
cek = cv2.flip(cek, 0)
img = cek

cv2.imshow('img', img)
reference = nib.load('segmentation-72.nii')
refe = reference.dataobj[:,:,71]
A = np.double(refe)
outImg = np.zeros(A.shape, np.double)
normalizedImg = cv2.normalize(A, outImg, 1.0, 0.0, cv2.NORM_MINMAX)
refe = normalizedImg*255
refe = np.uint8(refe)
M = cv2.getRotationMatrix2D((width / 2, height / 2), 90, 1)
refe = cv2.warpAffine(refe, M, (width, height))
refe = cv2.flip(refe, 0)
size = 15
kernel = np.ones((3,3), np.uint8)

mean, stdv = cv2.meanStdDev(img)
ut = mean + 1.5 * stdv
lt = mean + 0.5 * stdv
areaUnion = []
areaIntersect = []
areaImg = []
areaRef = []

for i in range(height):
    for j in range(width):
        if not (img[i][j] > lt and img[i][j] < ut):
            img[i][j] = 0
            cek[i][j] = 0

cv2.imshow('input', img)

result = img.copy()
result2 = cek.copy()
cv2.equalizeHist(img, img)
cv2.imshow('histeq', img)

img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
img = cv2.pyrMeanShiftFiltering(img, 10, 50)
img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
cv2.imshow('mean shift', img)
cv2.medianBlur(img, 11, img)
cv2.medianBlur(img, 11, img)
cv2.imshow('median', img)

mean, stdv = cv2.meanStdDev(img)
t = mean + 1.5 * stdv

for i in range(height):
    for j in range(width):
        if img[i][j] < t:
            img[i][j] = 0
cv2.imshow('thr2', img)

edges = cv2.Canny(img, 10, 100)
cv2.imshow('edges', edges)
edges2 = cv2.Canny(refe,150,200)

edges = cv2.dilate(edges, kernel)
im2, contours, hierarchy = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
im1, contours2, hierarchy2 = cv2.findContours(edges2, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

largest_index = 0
second_index = 1

for i in range(1, len(contours)):
    a = cv2.contourArea(contours[i], 0)
    largest_area = cv2.contourArea(contours[largest_index], 0)
    second_area = cv2.contourArea(contours[second_index], 0)
    if a > largest_area:
        second_index = largest_index
        largest_index = i
    elif a > second_area:
        second_index = i

ref = np.zeros((height, width, 1), np.uint8)
cnt = contours[largest_index]
cv2.drawContours(ref, cnt, -1, (255), -1)
# cek = cv2.cvtColor(cek, cv2.COLOR_GRAY2RGB)
# cv2.drawContours(cek, cnt, -1, (255,0,0), 4)
# # cv2.drawContours(cek, contours2, -1, (0,255,0), 4)
# cv2.drawContours(cek, contours2, 0, (0,0,255), 4)

cv2.equalizeHist(result, img)
cv2.imshow('hist', img)
img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
img = cv2.pyrMeanShiftFiltering(img, 10, 50)
img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
cv2.medianBlur(img, 11, img)
cv2.medianBlur(img, 11, img)
cv2.medianBlur(img, 11, img)

mean, stdv = cv2.meanStdDev(img)

mask = np.zeros((height + 2, width + 2), np.uint8)
cv2.floodFill(ref, mask, (0, 0), 255)
ref = cv2.bitwise_not(ref)
ref = cv2.erode(ref, kernel)
ref = cv2.erode(ref, kernel)
ref = cv2.erode(ref, kernel)

img = cv2.bitwise_and(img, img, mask=ref)
# cek = cv2.bitwise_and(cek, cek, mask=ref)

data = [x for x in img.ravel() if x != 0]
mean, stdv = np.mean(data), np.std(data)

# cv2.imshow('cek', cek)
cv2.imshow('tumor', img)
# cv2.imshow('ref', refe)

a = 0
for i in range(height):
    for j in range(width):
        if ref[i][j] == 255:
            a+= 0.703125 * 0.703125

print a

a=height/8
b=width/8

If = []
Ib = []
data = [x for x in img.ravel() if x != 0]
mean, stdv = np.mean(data), np.std(data)
for i in range(1, 9):
    for j in range(1, 9):
        n = (i - 1) * 8 + j
        ray = cek[b * (i - 1):b * i, a * (j - 1):a * j]
        data = [x for x in ray.ravel() if x != 0]
        m = np.mean(data)
        s = np.std(data)
        if s > 10:
            If = If + data
        else:
            Ib = Ib + data

cv2.waitKey(0)